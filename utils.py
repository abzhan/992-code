import os
import sys

def make_fcsv_from_list_of_points(fcsv_file_path, points, lmk):
    fcsv_header = '# Markups fiducial file version = 4.4\n' + \
    '# CoordinateSystem = 0\n' + \
    '# columns = id,x,y,z,ow,ox,oy,oz,vis,sel,lock,label,desc,associatedNodeID\n'
    
    with open(fcsv_file_path,'w') as fcsv:
        fcsv.write(fcsv_header)
    
    with open(fcsv_file_path,'a') as fcsv:
        for i, pt in enumerate(points):
            line = 'vtkMRMLMarkupsFiducialNode_' + str(i) + \
            ',' + str(pt[0]) + ',' + str(pt[1]) + ',' + str(pt[2]) + \
            ',0,0,0,1,1,1,0,' + lmk[i] + ',,vtkMRMLScalarVolume1\n'

            fcsv.write(line)    

    print('File '+fcsv_file_path+' has been created')
